import ccxt
import keys

ftx = ccxt.ftx({
'apiKey':keys.key,
'secret':keys.secret,
'enableRateLimit': True,
'headers': {'FTX-SUBACCOUNT': 'AdAstra'}, 
})

eth_balance = ftx.fetch_balance()['free']['ETH']
print(eth_balance)
print('')
print(ftx.fetch_balance())