import pickle

data = []
with open('file_data.pickle', 'rb') as f:
	data = pickle.load(f)

print(len(data))

def get_buy_and_hold(data):
	fee = 0.0001
	dollars = 100000
	coin = 0

	for tick in data:
		if dollars > 0:
			#print(f"bought at {tick['close']}")
			coin = dollars/tick['close']
			coin = coin - (coin*fee)
			dollars = 0

	if coin > 0:
		#print(f"sold at {tick['close']}")
		dollars = coin * tick['close']
		dollars = dollars - (dollars*fee)
		coin = 0

	finished = {'dollars':dollars,'coin':coin}
	return(finished)


def analyze_data(data,st_name):
	fee = 0.0001
	dollars = 100000
	coin = 0
	decisions_made = 0

	for tick in data:
		if dollars > 0:
			if tick[st_name] == 'up':
				#print('buying...')
				decisions_made += 1
				coin = dollars / tick['close']
				coin = coin - (coin*fee)
				dollars = 0
		elif coin > 0:
			if tick[st_name] == 'down':
				#print('selling...')
				decisions_made += 1
				dollars = coin * tick['close']
				dollars = dollars - (dollars*fee)
				coin = 0


	#print('results')
	if coin > 0:
		#print('selling...')
		dollars = coin * tick['close']
		dollars = dollars - (dollars*fee)
		coin = 0

	finished = {'dollars':dollars,'coin':coin,'decisions_made':decisions_made}
	return(finished)

def analyze_data_short(data,st_name):
	fee = 0.0001
	dollars = 100000
	coin = 0
	decisions_made = 0

	dollars_in = 0
	price_in = 0

	for tick in data:
		if dollars > 0:
			if tick[st_name] == 'down':
				#print('buying...')
				dollars_in = dollars
				price_in = tick['close']
				decisions_made += 1
				dollars = 0
		elif dollars == 0:
			if tick[st_name] == 'up':
				#print('selling...')
				decisions_made += 1
				price_out = tick['close']
				percent_change_down = (price_in - price_out) / price_in
				dollars_out = (1 + percent_change_down) * dollars_in
				dollars = dollars_out - (dollars*fee*2)


	#print('results')
	if dollars == 0:
		#print('selling...')
		decisions_made += 1
		price_out = tick['close']
		percent_change_down = (price_in - price_out) / price_in
		dollars_out = (1 + percent_change_down) * dollars_in
		dollars = dollars_out - (dollars*fee*2)

	finished = {'dollars':dollars,'coin':coin,'decisions_made':decisions_made}
	return(finished)

def analyze_data_both_directions(data,st_name):
	fee = 0.0001
	dollars = 100000
	coin = 0
	decisions_made = 0

	dollars_in = 0
	price_in = 0
	is_short = False
	is_long = False

	for tick in data:
		if tick[st_name] == 'up' and is_short == False and is_long == False:
			is_long = True
			#going long
			dollars_in = dollars
			price_in = tick['close']
			decisions_made += 1
			dollars = 0	

		elif tick[st_name] == 'down' and is_short == False and is_long == False:
			is_short = True
			#going short
			dollars_in = dollars
			price_in = tick['close']
			decisions_made += 1
			dollars = 0

		elif tick[st_name] == 'up' and is_long:
			pass

		elif tick[st_name] == 'down' and is_short:
			pass

		elif tick[st_name] == 'up' and is_short:
			is_short = False
			#closing short
			decisions_made += 1
			price_out = tick['close']
			percent_change_down = (price_in - price_out) / price_in
			dollars_out = (1 + percent_change_down) * dollars_in
			dollars = dollars_out - (dollars*fee*2)	

		elif tick[st_name] == 'down' and is_long:
			is_long = False
			#closing long
			decisions_made += 1
			price_out = tick['close']
			percent_change_down = (price_in - price_out) / price_in
			dollars_out = (1 - percent_change_down) * dollars_in
			dollars = dollars_out - (dollars*fee*2)

		else:
			print('wtf')

	if is_long:
		#closing long
		decisions_made += 1
		price_out = tick['close']
		percent_change_down = (price_in - price_out) / price_in
		dollars_out = (1 - percent_change_down) * dollars_in
		dollars = dollars_out - (dollars*fee*2)	
	elif is_short:
		#closing short
		decisions_made += 1
		price_out = tick['close']
		percent_change_down = (price_in - price_out) / price_in
		dollars_out = (1 + percent_change_down) * dollars_in
		dollars = dollars_out - (dollars*fee*2)	

	finished = {'dollars':dollars,'coin':coin,'decisions_made':decisions_made}
	return(finished)


def analyze_ST(st_name):
	print(st_name)
	k=1
	validations = []
	print('STARTING WITH $100,000 you would have finished with...')
	for i in range(0,k):
		k_unit_length = len(data) / k
		starting_index = int(k_unit_length * i)
		ending_index = int(k_unit_length * (i+1))
		data_segment = data[starting_index:ending_index]
		play_long = round(analyze_data(data_segment,st_name)['dollars'],2)
		short = round(analyze_data_short(data_segment,st_name)['dollars'],2)
		both = round(analyze_data_both_directions(data_segment,st_name)['dollars'],2)
		buy_hold = round(get_buy_and_hold(data_segment)['dollars'],2)
		date1 = data_segment[0]['date']
		date2 = data_segment[-1]['date']
		print(f'DATA FROM {date1} to  {date2}')
		print(f'going long with supertrend:            ${play_long}')
		print(f'going short with supertrend:           ${short}')
		print(f'going both directions with supertrend: ${both}')
		print(f'simply buying and holding:             ${buy_hold}')




