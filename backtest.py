import csv
import talib
import numpy
import pandas as pd
import pickle

def pull_data():
	data_directory = 'data/'
	#row[0] = Unix Timestamp
	#row[1] = Date
	#row[2] = Symbol
	#row[3] = Open
	#row[4] = High
	#row[5] = Low
	#row[6] = Close
	#row[7] = Volume

	data = []

	with open(f'{data_directory}ETH_1H.csv', newline='') as csvfile:
		reader = csv.reader(csvfile, delimiter=',', quotechar='|')
		i = 0
		for row in reader:
			try:
				data.append({'unix':float(row[0]),'open':float(row[3]),'high':float(row[4]),'low':float(row[5]),'close':float(row[6]),'date':row[1]})
			except:
				pass
	data = sorted(data, key = lambda item: item['unix'])

	with open(f'{data_directory}FTX_ETHUSDT_1h.csv', newline='') as csvfile:
		reader = csv.reader(csvfile, delimiter=',', quotechar='|')
		i = 0
		for row in reader:
			try:
				data.append({'unix':float(row[0]),'open':float(row[3]),'high':float(row[4]),'low':float(row[5]),'close':float(row[6]),'date':row[1]})
			except:
				pass

	data = sorted(data, key = lambda item: item['unix'])
	return(data)

def supertrend(high,low,close,length,multiplier):
    high = numpy.array(high)
    low = numpy.array(low)
    close = numpy.array(close)
    atr = talib.ATR(high,low,close,length)
    lower = []
    upper = []
    for a in range(0,len(atr)):
        upper.append(((high[a] + low[a]) / 2) + (multiplier * atr[a]))
        lower.append(((high[a] + low[a]) / 2) - (multiplier * atr[a]))
    return(lower,upper)

def read_supertrend(high,low,close,length=10,multiplier=3):
	lower, upper = supertrend(high,low,close,length,multiplier)

def EMA(df, base, target, period, alpha=False):
    """
    Function to compute Exponential Moving Average (EMA)
    
    Args :
        df : Pandas DataFrame which contains ['date', 'open', 'high', 'low', 'close', 'volume'] columns
        base : String indicating the column name from which the EMA needs to be computed from
        target : String indicates the column name to which the computed data needs to be stored
        period : Integer indicates the period of computation in terms of number of candles
        alpha : Boolean if True indicates to use the formula for computing EMA using alpha (default is False)
        
    Returns :
        df : Pandas DataFrame with new column added with name 'target'
    """

    con = pd.concat([df[:period][base].rolling(window=period).mean(), df[period:][base]])
    
    if (alpha == True):
        # (1 - alpha) * previous_val + alpha * current_val where alpha = 1 / period
        df[target] = con.ewm(alpha=1 / period, adjust=False).mean()
    else:
        # ((current_val - previous_val) * coeff) + previous_val where coeff = 2 / (period + 1)
        df[target] = con.ewm(span=period, adjust=False).mean()
    
    df[target].fillna(0, inplace=True)
    return df

def ATR(df, period, ohlc=['Open', 'High', 'Low', 'Close']):
    """
    Function to compute Average True Range (ATR)
    
    Args :
        df : Pandas DataFrame which contains ['date', 'open', 'high', 'low', 'close', 'volume'] columns
        period : Integer indicates the period of computation in terms of number of candles
        ohlc: List defining OHLC Column names (default ['Open', 'High', 'Low', 'Close'])
        
    Returns :
        df : Pandas DataFrame with new columns added for 
            True Range (TR)
            ATR (ATR_$period)
    """
    atr = 'ATR_' + str(period)

    # Compute true range only if it is not computed and stored earlier in the df
    if not 'TR' in df.columns:
        df['h-l'] = df[ohlc[1]] - df[ohlc[2]]
        df['h-yc'] = abs(df[ohlc[1]] - df[ohlc[3]].shift())
        df['l-yc'] = abs(df[ohlc[2]] - df[ohlc[3]].shift())
         
        df['TR'] = df[['h-l', 'h-yc', 'l-yc']].max(axis=1)
         
        df.drop(['h-l', 'h-yc', 'l-yc'], inplace=True, axis=1)

    # Compute EMA of true range using ATR formula after ignoring first row
    EMA(df, 'TR', atr, period, alpha=True)
    
    return df

def SuperTrend(df, period, multiplier, ohlc=['Open', 'High', 'Low', 'Close']):
    """
    Function to compute SuperTrend
    
    Args :
        df : Pandas DataFrame which contains ['date', 'open', 'high', 'low', 'close', 'volume'] columns
        period : Integer indicates the period of computation in terms of number of candles
        multiplier : Integer indicates value to multiply the ATR
        ohlc: List defining OHLC Column names (default ['Open', 'High', 'Low', 'Close'])
        
    Returns :
        df : Pandas DataFrame with new columns added for 
            True Range (TR), ATR (ATR_$period)
            SuperTrend (ST_$period_$multiplier)
            SuperTrend Direction (STX_$period_$multiplier)
    """

    ATR(df, period, ohlc=ohlc)
    atr = 'ATR_'+ str(period)
    st = 'ST_' + str(period) + '_' + str(multiplier)
    stx = 'STX' #+ str(period) + '_' + str(multiplier)
    
    """
    SuperTrend Algorithm :
    
        BASIC UPPERBAND = (HIGH + LOW) / 2 + Multiplier * ATR
        BASIC LOWERBAND = (HIGH + LOW) / 2 - Multiplier * ATR
        
        FINAL UPPERBAND = IF( (Current BASICUPPERBAND < Previous FINAL UPPERBAND) or (Previous Close > Previous FINAL UPPERBAND))
                            THEN (Current BASIC UPPERBAND) ELSE Previous FINALUPPERBAND)
        FINAL LOWERBAND = IF( (Current BASIC LOWERBAND > Previous FINAL LOWERBAND) or (Previous Close < Previous FINAL LOWERBAND)) 
                            THEN (Current BASIC LOWERBAND) ELSE Previous FINAL LOWERBAND)
        
        SUPERTREND = IF((Previous SUPERTREND = Previous FINAL UPPERBAND) and (Current Close <= Current FINAL UPPERBAND)) THEN
                        Current FINAL UPPERBAND
                    ELSE
                        IF((Previous SUPERTREND = Previous FINAL UPPERBAND) and (Current Close > Current FINAL UPPERBAND)) THEN
                            Current FINAL LOWERBAND
                        ELSE
                            IF((Previous SUPERTREND = Previous FINAL LOWERBAND) and (Current Close >= Current FINAL LOWERBAND)) THEN
                                Current FINAL LOWERBAND
                            ELSE
                                IF((Previous SUPERTREND = Previous FINAL LOWERBAND) and (Current Close < Current FINAL LOWERBAND)) THEN
                                    Current FINAL UPPERBAND
    """
    
    # Compute basic upper and lower bands
    df['basic_ub'] = (df[ohlc[1]] + df[ohlc[2]]) / 2 + multiplier * df[atr]
    df['basic_lb'] = (df[ohlc[1]] + df[ohlc[2]]) / 2 - multiplier * df[atr]

    # Compute final upper and lower bands
    df['final_ub'] = 0.00
    df['final_lb'] = 0.00
    for i in range(period, len(df)):
        df['final_ub'].iat[i] = df['basic_ub'].iat[i] if df['basic_ub'].iat[i] < df['final_ub'].iat[i - 1] or df[ohlc[3]].iat[i - 1] > df['final_ub'].iat[i - 1] else df['final_ub'].iat[i - 1]
        df['final_lb'].iat[i] = df['basic_lb'].iat[i] if df['basic_lb'].iat[i] > df['final_lb'].iat[i - 1] or df[ohlc[3]].iat[i - 1] < df['final_lb'].iat[i - 1] else df['final_lb'].iat[i - 1]
       
    # Set the Supertrend value
    df[st] = 0.00
    for i in range(period, len(df)):
        df[st].iat[i] = df['final_ub'].iat[i] if df[st].iat[i - 1] == df['final_ub'].iat[i - 1] and df[ohlc[3]].iat[i] <= df['final_ub'].iat[i] else \
                        df['final_lb'].iat[i] if df[st].iat[i - 1] == df['final_ub'].iat[i - 1] and df[ohlc[3]].iat[i] >  df['final_ub'].iat[i] else \
                        df['final_lb'].iat[i] if df[st].iat[i - 1] == df['final_lb'].iat[i - 1] and df[ohlc[3]].iat[i] >= df['final_lb'].iat[i] else \
                        df['final_ub'].iat[i] if df[st].iat[i - 1] == df['final_lb'].iat[i - 1] and df[ohlc[3]].iat[i] <  df['final_lb'].iat[i] else 0.00 
                 
    # Mark the trend direction up/down
    df[stx] = numpy.where((df[st] > 0.00), numpy.where((df[ohlc[3]] < df[st]), 'down',  'up'), numpy.NaN)

    # Remove basic and final bands from the columns
    df.drop(['basic_ub', 'basic_lb', 'final_ub', 'final_lb'], inplace=True, axis=1)
    
    df.fillna(0, inplace=True)

    return df

def up_down_super(data,len1,len2):
    df = pd.DataFrame(data)
    SuperTrend(df, len1, len2, ohlc=['open', 'high', 'low', 'close'])
    decision = df.STX.iloc[-1]
    return(decision)

data = pull_data()
print(len(data))

#start the backtest with 100,000 dollars
dollars = 100000
coins = 0
tax_rate = 0
portfolio_value = 100000


a = []

for hour in range(100,len(data)):
    print(f'{hour}/{len(data)}')
    test_info = data[hour-100:hour]
    trend = up_down_super(test_info,14,3)
    entry = data[hour]
    entry['supertrend143'] = trend
    trend = up_down_super(test_info,30,3)
    entry = data[hour]
    entry['supertrend303'] = trend
    trend = up_down_super(test_info,10,3)
    entry = data[hour]
    entry['supertrend103'] = trend
    trend = up_down_super(test_info,5,3)
    entry = data[hour]
    entry['supertrend53'] = trend
    trend = up_down_super(test_info,3,3)
    entry = data[hour]
    entry['supertrend33'] = trend
    trend = up_down_super(test_info,14,4)
    entry = data[hour]
    entry['supertrend144'] = trend
    trend = up_down_super(test_info,14,5)
    entry = data[hour]
    entry['supertrend145'] = trend
    trend = up_down_super(test_info,14,10)
    entry = data[hour]
    entry['supertrend1410'] = trend


    a.append(entry)


with open('file_data.pickle', 'wb') as handle:
    pickle.dump(a, handle, protocol=pickle.HIGHEST_PROTOCOL)


