import requests
import pandas as pd
import supertrend
import ccxt
import keys
import time
from ftx import FtxClient



def get_trend(symbol):

	historical = requests.get(f'https://ftx.com/api/markets/{symbol}/candles?resolution=3600').json()
	historical = pd.DataFrame(historical['result'])
	historical.drop(['startTime'], axis = 1, inplace=True)
	historical.head()

	open_ = historical['open']
	high_ = historical['high']
	low_ = historical['low']
	close = historical['close']


	trend = supertrend.up_down_super(historical,3,3)
	last_price = open_.iloc[-1]
	print(last_price)
	print(trend)
	return(trend)



def close_position(symbol):

	ftx = ccxt.ftx({
	'apiKey':keys.key,
	'secret':keys.secret,
    'enableRateLimit': True,
    'headers': {'FTX-SUBACCOUNT': keys.sub_name}, 
	})

	mypos = {'netSize':0}
	positions = ftx.fetchPositions()
	for pos in positions:
		if pos['future'] == symbol:
			mypos = pos

	if float(mypos['netSize']) > 0:
		print('closing long...')
		ftx.createMarketSellOrder(symbol,float(mypos['netSize']))
	elif float(mypos['netSize']) < 0:
		print('closing short...')
		ftx.createMarketBuyOrder(symbol,abs(float(mypos['netSize'])))
	else:
		print('no position held, no need to close...')
	print('waiting for close to fill...')
	time.sleep(10)
	return(0)


def get_collateral():

	c = FtxClient(
	    api_key=keys.key,
	    api_secret=keys.secret,
	    subaccount_name=keys.sub_name,
	)

	balance = c.get_account_info()['collateral']
	return(balance)

def check_to_change_position(symbol):

	ftx = ccxt.ftx({
	'apiKey':keys.key,
	'secret':keys.secret,
    'enableRateLimit': True,
    'headers': {'FTX-SUBACCOUNT': keys.sub_name}, 
	})

	mypos = {'netSize':0}
	positions = ftx.fetchPositions()
	for pos in positions:
		if pos['future'] == symbol:
			mypos = pos

	eth_price = 9999
	markets = ftx.fetchMarkets()
	for i in markets:
		if i['id'] == 'ETH/USDT':
			eth_price = i['info']['price']

	collateral_balance = float(get_collateral()) / float(eth_price)
	print(collateral_balance)
	collateral_allowed = 0.70 * collateral_balance

	trend = get_trend(symbol)
	if trend == 'up':
		if float(mypos['netSize']) > 0:
			print('already long... doing nothing')
		else:
			close_position(symbol)
			print('buying in...')
			order = ftx.createMarketBuyOrder(symbol,collateral_allowed)
			print(order)
			print('bought in')
	if trend == 'down':
		if float(mypos['netSize']) < 0:
			print('already short... doing nothing')
		else:
			close_position(symbol)
			print('selling...')
			# order = ftx.createMarketSellOrder(symbol,collateral_allowed)
			print(order)
			print('sold')




symbol = 'ETH-PERP'

check_to_change_position(symbol)


while True:
	try:
		check_to_change_position(symbol)
	except:
		print('there was an error in this trade...')
	time.sleep(30*60)

