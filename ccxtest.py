import ccxt
import keys

ftx = ccxt.ftx({
	'apiKey':keys.key,
	'secret':keys.secret,
    'enableRateLimit': True,
    'headers': {'FTX-SUBACCOUNT': 'AdAstra'}, 
	})

balance = ftx.fetchBalance()
print(balance)